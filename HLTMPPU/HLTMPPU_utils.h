// --*-- c++ --*--
#ifndef HLTMPPU_UTILS_H
#define HLTMPPU_UTILS_H
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <ctime>
#include <iostream>

namespace HLTMP{
  void printPtree(const boost::property_tree::ptree& args, std::string level) {
    boost::property_tree::ptree::const_iterator it,itend=args.end();
    level+=" ";
    for(it=args.begin();it!=itend;++it){
      std::string val(it->second.get_value<std::string>());
      boost::algorithm::trim(val);
      std::cout<<level<<it->first<<" : "<<val<<std::endl;
      printPtree(it->second,level);
    }
  }
  
  const std::string getTimeTag(){
    auto tnow=std::chrono::system_clock::now();
    std::time_t t=std::chrono::system_clock::to_time_t(tnow);
    char buff[100];
    auto countMS=std::chrono::duration_cast<std::chrono::milliseconds>(tnow.time_since_epoch()).count();
    auto countS=std::chrono::duration_cast<std::chrono::seconds>(tnow.time_since_epoch()).count();
    if (std::strftime(buff, sizeof(buff), "%Y-%b-%d %H:%M:%S", std::localtime(&t))){
      snprintf(buff+strlen(buff),100-strlen(buff),",%03ld ",countMS-countS*1000);
    }
    return std::string(buff);
  } 

  bool dump2File(const std::string &fname,const boost::property_tree::ptree& args){
    if(!fname.empty()){
      try{
        // boost::property_tree::xml_writer_settings<std::string> settings(" ",4);
        // boost::property_tree::write_xml(fname,args,std::locale(),settings);
        boost::property_tree::write_xml(fname,args,std::locale());
      }catch(boost::property_tree::xml_parser::xml_parser_error &ex){
        std::cerr<<HLTMP::getTimeTag()<<"Caught exception when writing xml to file \""
                 <<fname<<"\" exception is \""<<ex.what()<<"\""<<std::endl;
      }
      return false;
    }else{
      std::cerr<<HLTMP::getTimeTag()<<" File name can not be empty"<<std::endl;
      return false;
    }
    return true;
  }
  
  boost::property_tree::ptree getTreefromFile(const std::string &fileName){
    boost::property_tree::ptree pt;
    int fl=  boost::property_tree::xml_parser::no_comments|
      boost::property_tree::xml_parser::trim_whitespace;
    boost::property_tree::xml_parser::read_xml(fileName,pt,fl);
    return pt;
  }

}

#endif
