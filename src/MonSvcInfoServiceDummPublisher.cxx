#include "HLTMPPU/MonSvcInfoServiceDummPublisher.h"
#include "HLTMPPU/HLTMPPU_utils.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"
#include "ipc/partition.h"
#include "TObject.h"
#include "TH3.h"
#include "TH2.h"
#include "TH1.h"
#include "TH3F.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TH3C.h"
#include "TH2C.h"
#include "TH1C.h"
#include "TH3I.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TH3D.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TH3S.h"
#include "TH2S.h"
#include "TH1S.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TGraph2D.h"
#include "TGraph.h"
#include "ers/ers.h"
#include "oh/OHRootMutex.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <unordered_map>
#include "hltinterface/GenericHLTContainer.h"
#include "hltinterface/ContainerFactory.h"
#include "is/infodynany.h"


HLTMP::MonSvcInfoServiceDummPublisher::~MonSvcInfoServiceDummPublisher(){
}

HLTMP::MonSvcInfoServiceDummPublisher::MonSvcInfoServiceDummPublisher():MonSvcInfoService(){
  m_fillerThread=0;
  m_fillerWork=false;
  m_fillBin=1;
  m_fillPeriod=100;
  hltinterface::IInfoRegister::setInstance(this,true);
  std::cerr<<"registered MonSvcInfoServiceDummPublisher as IISRegister"<<std::endl;  
}


bool HLTMP::MonSvcInfoServiceDummPublisher::configure(const boost::property_tree::ptree &args){
  HLTMP::printPtree(args," MonSvcInfoServiceDummPublisher Config-> ");
  std::string dataPath="/home/kama/TDAQ/DAQ/DataFlow/HLTMPPU/test/histDump.xml";
  try{
    auto extras=args.get_child("HLTMonInfoImpl.extraParams").equal_range("parameter");
    for(auto it=extras.first;it!=extras.second;++it){
      std::vector<std::string> tokens;
      std::string data=std::string(it->second.data());
      boost::algorithm::split(tokens,data,boost::is_any_of("="));
      if(tokens.size()>1){
	std::cout<<data<<" "<<tokens.at(0)<<std::endl;
	if(tokens.at(0)==std::string("MonInfoDummyFilePath")){
	  dataPath=tokens.at(1);
	}
      }
    }
  }catch(boost::property_tree::ptree_bad_path &ex){
    std::cerr<<HLTMP::getTimeTag()<<"No ExtraParams "<<std::endl;
    ERS_DEBUG(1,"there is no extraParams, skipping");
  }
  parseHistogramDataFile(dataPath);
  return MonSvcInfoService::configure(args);
}


bool HLTMP::MonSvcInfoServiceDummPublisher::prepareWorker(const boost::property_tree::ptree &args){
  HLTMP::printPtree(args," MonSvcInfoServiceDummPublisher prepWorker-> ");
  startFiller();
  return MonSvcInfoService::prepareWorker(args);

}

bool HLTMP::MonSvcInfoServiceDummPublisher::finalizeWorker(const boost::property_tree::ptree& args ){
  stopFiller();
  return MonSvcInfoService::finalizeWorker(args);
}

bool HLTMP::MonSvcInfoServiceDummPublisher::finalize(const boost::property_tree::ptree&  args ){
  //monsvc::PublishingController::publish_now(m_publisher);
  bool fr=MonSvcInfoService::finalize(args);
  //They are cleaned by actual Monsvc
  // for(size_t t=0;t<m_dummHistos.size();t++{
  //   delete m_dummHistos.at(t);
  // }
  m_dummHistos.clear();
  return fr;
}




TH1* HLTMP::MonSvcInfoServiceDummPublisher::createHisto(const boost::property_tree::ptree &args,int type){
  
  const char* axisLabels[3]={"XAxis","YAxis","ZAxis"};
  
  std::string name;
  std::string title;
  std::string path;
  try{
    name=args.get_child("Name").get_value<std::string>();
    title=args.get_child("Title").get_value<std::string>();
    path=args.get_child("Path").get_value<std::string>();
  }catch(boost::property_tree::ptree_error &ex){
    std::cerr<<"Caught exception "<<ex.what()<<" for "<<std::endl;
    HLTMP::printPtree(args," InfoSvc exception-> ");
  }
  int nbins[3];
  double min[3];
  double max[3];
  bool hasLabels[3];
  int dim=(((type&7)==7)?3:(((type&7)==3)?2:1));
  TH1* hist(0);
  try{
    for(int i=0;i<dim;i++){
      const boost::property_tree::ptree ax=args.get_child(axisLabels[i]);
      nbins[i]=ax.get_child("Nbins").get_value<int>();
      min[i]=ax.get_child("Min").get_value<double>();
      max[i]=ax.get_child("Max").get_value<double>();
      hasLabels[i]=(ax.get_child_optional("BinLabels"))?true:false;
    }
  }catch(boost::property_tree::ptree_error &ex){
    std::cerr<<"Caught exception "<<ex.what()<<" for "<<std::endl;
    HLTMP::printPtree(args," InfoSvc exception-> ");
  }
  switch(type){
  case 65: //TH1F
    hist=new TH1F(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 67: //TH2F
    hist=new TH2F(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 71: //TH3F
    hist=new TH3F(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  case 33: //TH1I
    hist=new TH1I(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 35: //TH2I
    hist=new TH2I(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 39: //TH3I
    hist=new TH3I(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  case 9:  //TH1C
    hist=new TH1C(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 11: //TH2C
    hist=new TH2C(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 15: //TH3C
    hist=new TH3C(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  case 17: //TH1S
    hist=new TH1S(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 19: //TH2S
    hist=new TH2S(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 23: //TH3S
    hist=new TH3S(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  case 129: //TH1D
    hist=new TH1D(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 131: //TH2D
    hist=new TH2D(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 135: //TH3D
    hist=new TH3D(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  case 257: //TProfile
    hist=new TProfile(name.c_str(),title.c_str(),nbins[0],min[0],max[0]);
    break;
  case 259: //TProfile2D
    hist=new TProfile2D(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1]);
    break;
  case 263: //TProfile3D
    hist=new TProfile3D(name.c_str(),title.c_str(),nbins[0],min[0],max[0],nbins[1],min[1],max[1],nbins[2],min[2],max[2]);
    break;
  default:
    break;
  }
  if(!hist){
    std::cerr<<"Histogram generation failed type="<<type<<std::endl;
    return 0;
  }
  TAxis * hax[3];
  hax[0]=hist->GetXaxis();
  hax[1]=hist->GetYaxis();
  hax[2]=hist->GetZaxis();
  for(int i=0;i<dim;i++){
    if(!hasLabels[i])continue;
    const boost::property_tree::ptree ax=(args.get_child(axisLabels[i])).get_child("BinLabels");
    int b=1;
    BOOST_FOREACH(const boost::property_tree::ptree::value_type &v,ax){
      //std::cout<<"label "<<b<<"= \""<<v.second.get_value<std::string>()<<std::endl;
      hax[i]->SetBinLabel(b,v.second.get_value<std::string>().c_str());
      b++;
    }
  }
  registerTObject("Dummy",path+"/"+name,hist);
  return hist;
}

void HLTMP::MonSvcInfoServiceDummPublisher::parseHistogramDataFile(const std::string &path){

  //namespace pt=boost::property_tree;
  
  boost::property_tree::ptree tree;
  try{
    boost::property_tree::read_xml(path,tree);
  }catch(std::exception &ex){
    ERS_LOG("Warning caught exception "<<ex.what());
  }
  m_dummHistos.reserve(12000);
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, tree.get_child("HistDump")){
    int type=0;
    char d=v.first.at(2);
    char t=v.first.at(3);
    if(d=='1'){
      type=1;
    }else if(d=='2'){
      type=3;
    }else if(d=='3'){
      type=7;
    }else if(v.first.length()==8){
      type=1;
      t='P';
    }else{
      if(v.first.at(8)=='2'){
	type=3;
      }else{
	type=7;
      }
      t='P';
    }
    if(t=='C'){
      type+=8;
    }else if(t=='S'){
      type+=16;
    }else if(t=='I'){
      type+=32;
    }else if(t=='F'){
      type+=64;
    }else if(t=='D'){
      type+=128;      
    }else {
      type+=256;      
    }
    TH1* h=createHisto(v.second,type);
    m_dummHistos.push_back(h);
  }
}

void HLTMP::MonSvcInfoServiceDummPublisher::fillHistos(){
  size_t numhistos=m_dummHistos.size();
  while(m_fillerWork){
    m_fillBin=(m_fillBin&1)+1;
    for(size_t i=0;i<numhistos;i++){
      m_dummHistos[i]->AddBinContent(m_fillBin,1);
      m_dummHistos[i]->SetEntries(m_dummHistos[i]->GetEntries()+1);
    }
    try{
      boost::this_thread::sleep(boost::posix_time::seconds(m_fillPeriod));
    }catch(boost::thread_interrupted &ex){
      //don't need to sleep anymore
      if(!m_fillerWork){
	ERS_LOG("Filler thread finished. Returning");
	return;
      }
    }
  }
}

void HLTMP::MonSvcInfoServiceDummPublisher::startFiller(){
  if(m_fillerThread)return;
  m_fillerWork=true;
  ERS_LOG("Starting dummy Filler thread with period "<<m_fillPeriod);
  m_fillerThread=new boost::thread(&HLTMP::MonSvcInfoServiceDummPublisher::fillHistos,this);
}

void HLTMP::MonSvcInfoServiceDummPublisher::stopFiller(){
  m_fillerWork=false;
  if(!m_fillerThread)return;
  ERS_LOG("Stopping dummy filler thread");
  m_fillerThread->interrupt();
  //::sleep(1.5);
  m_fillerThread->join();
  delete m_fillerThread;
  m_fillerThread=0;
}

