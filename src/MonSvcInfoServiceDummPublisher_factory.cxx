#include "HLTMPPU/MonSvcInfoServiceDummPublisher.h"

extern "C" hltinterface::IInfoRegister* create_hltmp_infoservice(){
  return new HLTMP::MonSvcInfoServiceDummPublisher();
}

extern "C" void destroy_hltmp_infoservice(hltinterface::IInfoRegister* i){
  HLTMP::MonSvcInfoServiceDummPublisher* k=reinterpret_cast<HLTMP::MonSvcInfoServiceDummPublisher*>(i);
  delete k;
}
