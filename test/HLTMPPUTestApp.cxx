#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <boost/program_options.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/thread.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include "ipc/partition.h"
#include "ipc/core.h"
#include "hltinterface/HLTInterface.h"
#include <dlfcn.h>
#include "HLTMPPU/Issues.h"


using creator = hltinterface::HLTInterface* (*)(void);
using destroyer = void (*)(hltinterface::HLTInterface*);
namespace po = boost::program_options;
pid_t myPid;
boost::thread *rcEmu=0;

void handler(int signal) {
  (void) signal;
  std::cerr<<__PRETTY_FUNCTION__<<" PID= "<<getpid()<<" got signal "<<signal<<std::endl;
  ERS_LOG("PID= "<<getpid()<<" Received signal "<<signal<<", handler was registered by pid="<<myPid);
	  //ERS_LOG("PID= "<<getpid()<<" Received signal handler registered by pid"<<myPid<<std::endl;
  //hltRec->stop();
  // waitCond.notify_all();
}

void childTerminated(int signal) {
  (void) signal;
  std::cerr<<__PRETTY_FUNCTION__<<" PID= "<<getpid()<<" terminated with signal "<<signal <<std::endl;
  ERS_LOG("PID= "<<getpid()<<" Received signal "<<signal<<", handler was registered by pid="<<myPid);
}

void sahandler(int sig,siginfo_t * si,void* /*vp*/){
  std::cerr<<__PRETTY_FUNCTION__<<" Child "<<getpid()<<" terminated with signal "<<sig <<std::endl;
  if(!si){
    std::cerr<<__PRETTY_FUNCTION__<<" siginfo_t is NULL "<<std::endl;
    return;
  }
  std::cerr<<__PRETTY_FUNCTION__<<"signo="<<si->si_signo
	   <<"  , errno="<<si->si_errno<<std::endl
	   // <<"  , trapno="<<si->si_trapno<<std::endl
    	   <<"  , pid="<<si->si_pid<<std::endl
	   <<"  , uid="<<si->si_uid<<std::endl
	   <<"  , status="<<si->si_status<<std::endl;
  
  std::cerr<<__PRETTY_FUNCTION__<<" si_code is =";
  switch (si->si_code){
  case CLD_EXITED:
    std::cerr<<"CLD_EXITED"<<std::endl;
    break;
  case CLD_KILLED:
    std::cerr<<"CLD_KILLED"<<std::endl;
    break;
  case CLD_DUMPED:
    std::cerr<<"CLD_DUMPED"<<std::endl;
    break;
  case CLD_TRAPPED:
    std::cerr<<"CLD_TRAPPED"<<std::endl;
    break;
  case CLD_STOPPED:
    std::cerr<<"CLD_STOPPED"<<std::endl;
    break;
  case CLD_CONTINUED:
    std::cerr<<"CLD_CONTINUED"<<std::endl;
    break;
  default:
    std::cerr<<"OTHER CODE = "<<si->si_code<<std::endl;
    break;
  }
}

boost::property_tree::ptree getTreefromFile(const std::string &fileName){
  boost::property_tree::ptree pt;
  int fl=  boost::property_tree::xml_parser::no_comments|
    boost::property_tree::xml_parser::trim_whitespace;
  boost::property_tree::xml_parser::read_xml(fileName,pt,fl);
  return pt;
}

void doTest(hltinterface::HLTInterface *hltmppu,int sleepTime,int numLoops=1,bool unconf=false){
  if(!unconf){
    ERS_LOG("Calling configure");  
    hltmppu->configure(getTreefromFile("configure.xml"));
    ERS_LOG("Calling connect");  
    hltmppu->connect(getTreefromFile("connect.xml"));
  }

  for (int i=0;i<numLoops;i++){
    if(unconf){
      ERS_LOG("Calling configure");  
      hltmppu->configure(getTreefromFile("configure.xml"));
      ERS_LOG("Calling connect");  
      hltmppu->connect(getTreefromFile("connect.xml"));
    }
    ERS_LOG("Calling prepareForRun");  
    hltmppu->prepareForRun(getTreefromFile("prepareForRun.xml"));
    ERS_LOG("prepareForRun done! Sleeping for some time ");
    if(getpid()!=myPid){//in children
      if(rcEmu!=0)rcEmu->detach();
      return;
    }

    uint maxSleep=sleepTime;
    while((maxSleep=sleep(maxSleep))>0);

    ERS_LOG("Calling stopRun "<<myPid);
    hltmppu->stopRun(getTreefromFile("stopRun.xml"));
    //ERS_LOG("Calling prepareForRun");  
    if(unconf){
      boost::property_tree::ptree dumm;
      ERS_LOG("Calling disconnect "<<myPid);
      hltmppu->disconnect(dumm);
      ERS_LOG("Calling unconfigure "<<myPid);
      hltmppu->unconfigure(dumm);
    }
  }
  // hltmppu->prepareForRun(getTreefromFile("prepareForRun.xml"));
  // std::cout<<__PRETTY_FUNCTION__<<"Done. Sleeping for some time "<<std::endl;
  // sleep(sleepTime/2.);
  // //sleep can be interrupted by a signal
  // sleep(sleepTime/2.);
  // ERS_LOG("Calling stopRun "<<myPid);
  // hltmppu->stopRun(getTreefromFile("stopRun.xml"));
  // ERS_LOG("Calling unconfigure "<<myPid);
  // hltmppu->unconfigure(getTreefromFile("stopRun.xml"));
  // ERS_LOG("Calling configure");  
  // hltmppu->configure(getTreefromFile("configure.xml"));
  // ERS_LOG("Calling connect");  
  // hltmppu->connect(getTreefromFile("connect.xml"));
  // ERS_LOG("Calling prepareForRun");  
  // hltmppu->prepareForRun(getTreefromFile("prepareForRun.xml"));
  // std::cout<<__PRETTY_FUNCTION__<<"Done. Sleeping for some time "<<std::endl;
  // sleep(sleepTime/2.);
  // ERS_LOG("Calling stopRun "<<myPid);
  // hltmppu->stopRun(getTreefromFile("stopRun.xml"));
  return;
}

int main(int argc, char * argv[]){
  std::string parentName(""), segmentName(""), name("");
  std::string hltdll;
  int sleepTime;
  bool doThreaded=false;
  bool unconf=false;
  int nLoops=1;
  myPid=getpid();

  setenv("TDAQ_APPLICATION_NAME","HLTMPPU_testApp",1);
  setenv("TDAQ_PARTITION","part-HLTMPPU_test",1);

  try {
    po::options_description desc("This program is loads xml files for each step and calls "\
				 "\nHLTMPPU steering methods from the given library.\n"\
				 "requires \"configure.xml\", \"connect.xml\", \"prepareForRun.xml\"\n"\
				 " and \"stopRun.xml\" to be present in the current directory");
    desc.add_options()
      ("name,n"            , po::value<std::string>(&name)->default_value("asd")              , "Name of the application")
      ("dllName,d"         , po::value<std::string>(&hltdll)->default_value("libHLTMPPU.so")  , "Name of the dll containing implementation")
      ("sleep-duration,s"  , po::value<int>(&sleepTime)->default_value(30)              , "Amount of time to sleep after prepareForRun step")
      ("numLoops,l"  , po::value<int>(&nLoops)->default_value(1)              , "number of times to go up and down in FSM")
      ("threaded,t" , "Flag for running with threads (similar to HLTRC)")
      ("unconfigure,u" , "do configure<->unconfigure fsm loop instead of prepareForRun<->stopRun. ")
      ("help,h"                                                                                  , "Print help message")
      ;
    
    std::cout<<"Starting HLTMTestApp "<<std::endl;
    std::cout<<"Input parameters "<<std::endl;
    for(int i=0;i<argc;i++){
      std::cout<<" "<<i<<" \""<<argv[i]<<"\""<<std::endl;
    }

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    std::cout<<"Got input parameters "<<std::endl;
    po::basic_parsed_options<char> bop=po::parse_command_line(argc, argv, desc);
    for(size_t t=0;t<bop.options.size();t++){
      for(size_t k=0;k<bop.options.at(t).value.size();k++){
	std::cout<<" "<<bop.options.at(t).string_key<<" ["<<k<<"] = "<<bop.options.at(t).value.at(k)<<std::endl;
      }
    }
    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    if(!vm.count("name")) {
      ers::error(HLTMPPUIssue::CommandLineIssue(ERS_HERE,"Missing application name!"));
      std::cout << desc << std::endl;
      return EXIT_FAILURE;
    }
    if(hltdll.length()==0) {
      ers::error(HLTMPPUIssue::CommandLineIssue(ERS_HERE,"Missing dll name!"));
      std::cout << desc << std::endl;
      return EXIT_FAILURE;
    }
    if(vm.count("threaded")){
      doThreaded=true;
    }
    if(vm.count("unconfigure")){
      unconf=true;
    }
  }catch(std::exception& ex) {
    ers::error(HLTMPPUIssue::CommandLineIssue(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }
  //signal handling
  
  bool IPCFailed=false;
  std::cout<<"Initializing IPC"<<std::endl;
  try{
    IPCCore::init(argc,argv);
  }catch(std::exception &ex){
    std::cerr<<"IPCinit failed \""<<ex.what()<<"\"."<<std::endl;
    IPCFailed=true;
  }
  if(IPCFailed){
    std::cerr<<"Need IPC to be able to run exiting"<<std::endl;
    return EXIT_FAILURE;
  }
  struct sigaction act;
  memset (&act, '\0', sizeof(act));
  act.sa_sigaction=&sahandler;
  act.sa_flags=SA_SIGINFO;
  if(sigaction(SIGCHLD,&act,NULL)<0){
    std::cerr<<"Error setting signal handler"<<std::endl;
    ers::error(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,(std::string(" can't set SIGCHLD. Exiting ")).c_str()));
    return EXIT_FAILURE;
  }

  //::signal(SIGKILL,handler);
  ::signal(SIGTERM,childTerminated);

  void * myDllHandle=dlopen(hltdll.c_str(),RTLD_LAZY|RTLD_GLOBAL);
  if(!myDllHandle){
    const char* errmsg=dlerror();
    if(errmsg){
      ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,(std::string("can't find ")+hltdll+" "+std::string(errmsg)).c_str()));
    }else{
      ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,(std::string("can't find ")+hltdll).c_str()));
    }
    return EXIT_FAILURE;
  }
  using creator_dlsym = creator(*)(void *, const char*);
  creator c=reinterpret_cast<creator_dlsym>(dlsym)(myDllHandle,"create_interface");
  const char* dlsymError=dlerror();
  if(dlsymError){
    ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,"can't import create_interface function!"));
    return EXIT_FAILURE;
  }
  using dest_dlsym = destroyer(*)(void *, const char*);
  destroyer d=reinterpret_cast<dest_dlsym>(dlsym)(myDllHandle,"destroy_interface");
  dlsymError=dlerror();
  if(dlsymError){
    ers::error(HLTMPPUIssue::DLLIssue(ERS_HERE,"can't import destroy_interface function!"));
    return EXIT_FAILURE;
  }

  ERS_LOG("Constructing HLTInterface");
  hltinterface::HLTInterface *hltmppu=c();
  if(doThreaded){
    ERS_LOG("Creating new thread");
    rcEmu=new boost::thread(doTest,hltmppu,sleepTime,nLoops,unconf);
    ERS_LOG("Waiting for the thread to join");
    rcEmu->join();
    delete rcEmu;
  }else{
    try{
      doTest(hltmppu,sleepTime,nLoops,unconf);
    }catch(ers::Issue  &ex) {
      std::cerr<<"Got an ers issue "<<std::endl;
      ers::fatal(ex);
      return EXIT_FAILURE;
    }catch(std::exception &ex){
      std::cerr<<"Got an exception"<<ex.what()<<std::endl;
      char buff[800];
      snprintf(buff,800,"Caught an unexpected exception \"%s\" exiting!",ex.what());
      ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,buff));
      return EXIT_FAILURE;
    }
  }
  std::cout<<__PRETTY_FUNCTION__<<"Main process completed exiting "<<std::endl;
  d(hltmppu);
  
  sleep(0.1);
}
